﻿#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <direct.h>

void main()
{
	LPCSTR f;
	int ok;

	char* path = _getcwd(NULL, 0);
	if (path == NULL)
	{
		perror("_getcwd error! ERROR:");
		system("pause");
		exit(0);
	}
	printf("Here's the path:   %s\n", path);

	f = (LPCSTR)path;
	
	ok = DeleteFile(f);
	if (!ok)
	{
		printf("\nfile deletion failed! ERROR: %d\n", GetLastError()); //DOES return error! can not delete itself.
		printf("\nProbably printed error 5.\nError 5 means \"access is denied\".\nIt doesn't let us delete the file because the program is still running!\n\n");
		system("pause");
		exit(0);
	}
	printf("The file deleted!\n"); //line that probably won't be printed..
	
	system("pause");
}