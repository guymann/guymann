#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <direct.h>
#include <FileAPI.h>
#include <WinBase.h>
#include <tchar.h>
#include <strsafe.h>

void main()
{
	char buff[] = "del Task2.3.exe\ndel deleter.bat";
	LPDWORD written=NULL;

	HANDLE DeleterHand;
	DeleterHand = CreateFile("deleter.bat", GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (DeleterHand == INVALID_HANDLE_VALUE)
	{
		printf("Creating file error! ERROR: %d.\n", GetLastError());
		system("pause");
		exit(0);
	}
	
	WriteFile(DeleterHand, buff, strlen(buff), written, NULL);

	system("pause");
}
/**************************************/
//youll have to compile that file
//run the exe file
//the deleter will be created
//open the deleter and it will delete both of the files.