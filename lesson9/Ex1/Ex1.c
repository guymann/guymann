#include <stdlib.h>
#include <stdio.h>
#include <string.h>
/*  name:   malloc.
    input:  size.
    output: on success - pointer to the allocated memory, in error or input is zero - NULL. 
*/
void* myMalloc(size_t size)
{
	void* m;
	if(m=sbrk(size) == -1)
	{	
		return NULL;
	}	
	else
	{
		return m;
	}
}
int main()
{
	int i;	

	//array size
	int num = (int)myMalloc(sizeof(int));
	if(arr==NULL)
	{
		printf("ERROR in malloc :-(\n");
		return 0;
	}	

	//array
	puts("\nEnter size please:  ");
	scanf("%d", &num);
	int* arr = (int*)myMalloc(sizeof(int)*num);
	if(arr==NULL)
	{
		printf("ERROR in malloc :-(\n");
		return 0;
	}
	
	//array input
	for(i=0; i<num; i++)
	{
		printf("Enter num to arr[%d]: ", i);
		scanf("%d", &arr[i]);
	}

	//array output
	puts("Here is the array that you entered:");
	for(i=0; i<num; i++)
	{
		printf("arr[%d]: %d\n", i, arr[i]);
	}
	
	return 0;	
}
