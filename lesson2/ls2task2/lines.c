#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int fileLinesCounter(FILE* f)
{
	int counter = 1;
	int ch;
	fseek(f, 0, SEEK_SET);
	ch = fgetc(f);
	while((ch != -1))
	{
		if(ch == 10)
		{
			counter++;
		}
		ch = fgetc(f);
	}
	return (counter);
}

int randLine(int max)
{
	int randNum;
	srand(time(NULL));
	randNum = rand() % max + 1;
	return (randNum);
}

void printFileRandLine(int randLine, FILE* f)
{
	int ch, counter=0;
	fseek(f, 0, SEEK_SET);
	ch = fgetc(f);
	while(counter != (randLine-1))
	{
		if(ch == 10)
		{
			counter += 1;
		}
		ch = fgetc(f);
	}
	while(ch != 10 && ch != -1)
	{
		printf("%c", ch);
		ch = fgetc(f);
	}
	puts("");
}

int main(int argc, char** argv)
{
	FILE* f;
	char fileName[200];
	int linesNum, random;

	strcpy(fileName, argv[1]);	
	f = fopen(fileName, "r+");
	if(!f)
	{
		puts("Error while file opening!");
		return (0);
	}
	fseek(f, 0, SEEK_SET);
	
	linesNum = fileLinesCounter(f);

	random = randLine(linesNum);
	
	printFileRandLine(random, f);

	fclose(f);
	return(0);
}
