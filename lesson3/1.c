#include <unistd.h>
#include <sys/sysinfo.h>
#include <sys/syscall.h>
#include "helper.h"

#define STDOUT 1
#define STDIN 0

void monkeyExit()
{
	sys_exit();
}

void monkeyId()
{
	char * p;	
	int id;
	id = getuid();
	p = __itoa(id);
	syscall(SYS_write, STDOUT, p, slen(p));
}

void monkeyUptimeProc()
{
	struct sysinfo *info;
	char * p;	
	long upt;
	sysinfo(info);
	upt = info->uptime;
	p = __itoa(upt);
	syscall(SYS_write, STDOUT, p, slen(p));
}

int main()
{
	int choice=5;
	int i;
	char choiceChar[3];	
	fun_desc arr[4];
	
	arr[0].name = "   0: exit\n";
	arr[0].fun = monkeyExit;
	arr[1].name = "   1: prints your id\n";
	arr[1].fun = monkeyId;
	arr[2].name = "   2: prints the uptime\n";
	arr[2].fun = monkeyUptimeProc;
	while(1)
	{		
		for(i = 0; i < 3; i++)
		{	
			syscall(SYS_write, STDOUT, arr[i].name, slen(arr[i].name));
		}		
		syscall(SYS_read, STDOUT, choiceChar, 2);	
		if(choiceChar == '0')
			choice = 0;
		else if(choiceChar == '1')
			choice = 1;
		else if(choiceChar == '2')
			choice = 2;
		else
			return 0;
		arr[choice].fun();
	}
	return 0;
}
