#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int addr5 = 24;
int addr6;

int addr4();

int main(int argc, char** argv)
{
	static int addr1;
	int addr2;
	int addr3[3];
	char* addr7="?";
	int * addr8 = (int*)malloc(12);
	printf("%p\n",&addr1); //data segment - bss
	printf("%p\n",&addr2); //stack
	printf("%p\n",&addr3); //stack
	printf("%p\n",addr4); //data segment - data
	printf("%p\n",&addr5); //data segment - bss
	printf("%p\n",&addr6); //data segment - bss
	printf("%p\n",addr7); //data segment - data
	printf("%p\n",addr8); //data - heap
	return 0;
}

int addr4()
{
	return -1;
}
