#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/limits.h>
#include "LineParser.h"
#include <string.h>
#include <sys/wait.h>

int execute(cmdLine *pCmdLine)
{
	int m, status;
	pid_t pid, returned;
	if((pid = fork()) == 0) //child
	{
		m = execvp(pCmdLine->arguments[0],
		pCmdLine->arguments);		
		if(m==-1)
		{
			perror("**ERROR");
			freeCmdLines(pCmdLine);
			exit(0);
		}
	}
	else if(pCmdLine->blocking == 0) //father, needs to wait
	{
		waitpid(pid, &status, WUNTRACED);			
	}
	return 0;
}

void myChdir(cmdLine *pCmdLine)
{
	int i, m;	
	int originalLen, slashCounter1=0, slashCounter2=0;	
	char originalPath[PATH_MAX];
	char newPath[PATH_MAX];
	getcwd(originalPath, PATH_MAX);
	originalLen = strlen(originalPath);
	if(strcmp(pCmdLine->arguments[1], "..") == 0)
	{
		for(i=originalLen-1; i>=0; i--)
		{
			if(originalPath[i] == '/')
			{
				slashCounter1++;
			}
		}
		for(i=0; slashCounter2 < slashCounter1; i++)
		{
			newPath[i] = originalPath[i];
			if(originalPath[i] == '/')
			{
				slashCounter2++;
			}
		}
		newPath[i-1] = NULL;
	}
	else
	{
		strcpy(newPath, pCmdLine->arguments[1]);
	}
	m = chdir(newPath);		
	if(m)
	{
		printf("ERROR!");
	}
}

void doWhatUserAsk(cmdLine *pCmdLine)
{
	int i, Quit, MyEcho, m, Cd;	
	Quit = strcmp(pCmdLine->arguments[0], "quit");	
	MyEcho = strcmp(pCmdLine->arguments[0], "myecho");	
	Cd = strcmp(pCmdLine->arguments[0], "cd");
	if(!Quit) //if does quit, strcmp return false (0) if same.
	{
		freeCmdLines(pCmdLine);		
		exit (0);
	}
	else if(!MyEcho) //if does myecho...
	{
		for(i=1; i<pCmdLine->argCount; i++)
		{
			printf("%s ", pCmdLine->arguments[i]);
		}
		puts("");
	}
	else if(!Cd) //if does cd...
	{
		myChdir(pCmdLine);
	}
	else
	{
		m = execute(pCmdLine);
	}
}

void main()
{
	cmdLine * pCmdLine;	
	char input[PATH_MAX];	
	char path[PATH_MAX];
	int pid, waitStatus, childStatus, ret;
	while(1)
	{
		getcwd(path, PATH_MAX); //get the path
		printf("\n%s>>  ", path);  //print the path
		fgets(input, 2049, stdin); 	//get user's input
		pCmdLine = parseCmdLines(input); //parse the input
		doWhatUserAsk(pCmdLine);
		freeCmdLines(pCmdLine);	

	}
}
