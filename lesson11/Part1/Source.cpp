#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <string.h>
#include <windows.h>
using namespace std;

void main(int argc, char** argv)
{
	bool ok;
	DWORD pid = atoi(argv[1]);
	
	HANDLE hand = OpenProcess(PROCESS_TERMINATE, true, pid);
	if (hand == NULL)
	{
		printf("ERROR: %u.\nGood-Bye!\n", GetLastError());
		exit(0);
	}
		
	ok = TerminateProcess(hand, 1);
	if (!ok)
	{
		printf("ERROR: %u.\nGood-Bye!\n", GetLastError());
		exit(0);
	}

	ok = CloseHandle(hand);
	if (!ok)
	{
		printf("ERROR: %u.\nGood-Bye!\n", GetLastError());
		exit(0);
	}
	printf("Process number %s is not alive anymore!\n", argv[1]);
	system ("pause");
}
